



let getCube = number => {
	number ** 3;
	console.log(`Your number ${number} cubed is ${number ** 3}`)
}

getCube(2);

let address = [`Philippines`,`Luzon`,`Benguet`,`Baguio`]

let [country,island,province,city] = address;

console.log(`My country is ${country}, I am from ${island}, my house is in ${province}-${city}`);

let animal = {
	type: "dog",
	breed: "golden retriver",
	age: 2,
	favoriteFood: "cat"
}

let {type , breed, age, favoriteFood} = animal;

console.log(`I am a ${type}, speficically a ${breed}, I am ${age} years old, I like to eat ${favoriteFood}`);

let arrayNumbers = [3,5,87,2,6,9];

arrayNumbers.forEach(numbers=>console.log(numbers))

let reduced = arrayNumbers.reduce((a,b)=> (a + b));
console.log(reduced);

class dog {
	constructor(dogName,dogAge,dogBreed){
		this.name = dogName;
		this.age = dogAge;
		this.breed = dogBreed;
	}
}

let mydog = new dog(`Bruno`,2,`Labrador`);

console.log(mydog)